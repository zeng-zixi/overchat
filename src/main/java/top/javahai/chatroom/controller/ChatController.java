package top.javahai.chatroom.controller;

import com.zaxxer.hikari.util.FastList;
import org.apache.commons.lang3.StringUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.javahai.chatroom.entity.FileList;
import top.javahai.chatroom.entity.User;
import top.javahai.chatroom.service.UserService;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Overchat
 * @date 2021/9/16 - 21:32
 */
@RestController
@RequestMapping("/chat")
public class ChatController {


    @Autowired
    UserService userService;



    @GetMapping("/users")
    public List<User> getUsersWithoutCurrentUser() {
        System.out.println("用户*****************"+userService.getUsersWithoutCurrentUser());
        return userService.getUsersWithoutCurrentUser();
    }


}
