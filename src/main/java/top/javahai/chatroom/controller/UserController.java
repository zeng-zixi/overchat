package top.javahai.chatroom.controller;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;
import top.javahai.chatroom.entity.RespBean;
import top.javahai.chatroom.entity.RespPageBean;
import top.javahai.chatroom.entity.User;
import top.javahai.chatroom.service.UserService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * (User)表控制层
 *
 * @author Overchat
 * @since 2021-09-11 11:37:09
 */
@RestController
@RequestMapping("/user")
public class UserController {
    /**
     * 服务对象
     */
    @Resource
    private UserService userService;

    /**
     * 注册操作
     */
    @PostMapping("/register")
    public RespBean addUser(@RequestBody User user){
        if (userService.insert(user)==1){
            return RespBean.ok("注册成功！");
        }else{
            return RespBean.error("注册失败！");
        }
    }

    /**
     * 注册操作，检查用户名是否已被注册
     * @param username
     * @return
     */
    @GetMapping("/checkUsername")
    public Integer checkUsername(@RequestParam("username")String username){
        return userService.checkUsername(username);
    }

    /**
     * 注册操作，检查昵称是否已被注册
     * @param nickname
     * @return
     */
    @GetMapping("/checkNickname")
    public Integer checkNickname(@RequestParam("nickname") String nickname){
        return userService.checkNickname(nickname);
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public User selectOne(Integer id) {
        return this.userService.queryById(id);
    }

    /**
     * 分页得到所有用户，可以用来查寻当前页、关键词搜索
     * @param page  页数，对应数据库查询的起始行数
     * @param size  数据量，对应数据库查询的偏移量
     * @param keyword 关键词，用于搜索
     * @param isLocked  是否锁定，用于搜索
     * @return
     */
    @GetMapping("/")
    public RespPageBean getAllUserByPage(@RequestParam(value = "page",defaultValue = "1") Integer page,
                                         @RequestParam(value = "size",defaultValue = "10") Integer size,
                                         String keyword,Integer isLocked){
        return userService.getAllUserByPage(page,size,keyword,isLocked);
    }

    /**
     * 更新用户的锁定状态
     * @author luo
     * @param id
     * @param isLocked
     * @return
     */
    @PutMapping("/")
    public RespBean changeLockedStatus(@RequestParam("id") Integer id,@RequestParam("isLocked") Boolean isLocked){

      if (userService.changeLockedStatus(id,isLocked)==1){
          return RespBean.ok("更新成功！");
      }else {
          return RespBean.error("更新失败！");
      }
    }
//    @PostMapping("/fileDowload/{filename}")
//    public RespBean fileDowload(@PathVariable String filename) throws IOException, URISyntaxException, InterruptedException {
//        FileSystem fs = FileSystem.get(new URI("hdfs://192.168.152.138:9000"), new Configuration(), "root");
//        InputStream in = fs.open(new Path("/"+filename));
//        FileOutputStream out = new FileOutputStream(new File("d:/BigData/"+filename),true);
//        IOUtils.copyBytes(in, out, 4096, true);
//        File f = new File("D:/BigData/"+filename);
//
//        if(f.exists()) {
//            return RespBean.ok("下载成功！");
//        }
//        else{
//            return RespBean.error("下载失败！");
//        }
//
//    }



    /**
     * 删除单一用户
     * 批量删除用户
     * @author luo
     * @param ids
     * @return
     */
    @DeleteMapping("/")
    public RespBean deleteUserByIds(@RequestParam("ids") Integer[] ids){
        if (userService.deleteByIds(ids)==ids.length){
            return RespBean.ok("删除成功！");
        }else{
            return RespBean.error("删除失败！");
        }
    }
}
