package top.javahai.chatroom.controller;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import top.javahai.chatroom.entity.ImgFormat;
import top.javahai.chatroom.service.ScreenShotService;


import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestController
public class ScreenShotController {
    @Value("${spring.hadoop.fsUri}")     private String urlPost;
    @Autowired
    private ScreenShotService screenShotService;
/*
    全屏截图
 */
    @GetMapping("/screenShot")
    @CrossOrigin  //允许请求跨域
    public List<Object> screenShot() throws IOException, URISyntaxException, InterruptedException {
        ImgFormat imgFormat=new ImgFormat();
       List<Object> map = new ArrayList<>();
        map=screenShotService.snapShot(imgFormat);

        FileSystem fileSystem = FileSystem.get(new URI(urlPost), new Configuration(), "root");
        // 创建输入流，参数指定文件输出地址
        imgFormat.getSaveAddress();
        InputStream in = new FileInputStream(imgFormat.getSaveAddress());

        // 调用create方法指定文件上传，参数HDFS上传路径
        OutputStream out = fileSystem.create(new Path("/"+imgFormat.getLgTime()+"." + imgFormat.getImageFormat()));
        // 使用Hadoop提供的IOUtils，将in的内容copy到out，设置buffSize大小，是否关闭流设置true
        IOUtils.copyBytes(in, out, 4096, true);
        return map;
    }
}
