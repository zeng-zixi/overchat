package top.javahai.chatroom.entity;

/**
 * @author OverChat
 * @date
 */
public class FIleSuess {
    private String url;
    private String filename;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }
}
