package top.javahai.chatroom.entity;

import java.io.Serializable;

public class FileList implements Serializable {
    private String filename;
    private String date;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

//    @Override
//    public String toString() {
//        return "FileList{" +
//                "filename='" + filename + '\'' +
//                ", date11='" + date111 + '\'' +
//                '}';
//    }
}
