package top.javahai.chatroom.entity;

import java.io.Serializable;


public class ImgFormat implements Serializable {

    private String fileName;//文件地址的前缀

//    默认图片保存在resources/BigImg路径下
    private String defaultName = "src/main/resources/BigImg/";

    private String imageFormat;//图像文件的格式
    private String LgTime;
    private String defaultImageFormat = "png"; //默认的图片格式

    private String saveAddress=""; //保存地址
    private String msg;   //消息

    public ImgFormat(String fileName, String defaultName, String imageFormat, String defaultImageFormat, String saveAddress, String msg) {
        this.fileName = fileName;
        this.defaultName = defaultName;
        this.imageFormat = imageFormat;
        this.defaultImageFormat = defaultImageFormat;
        this.saveAddress = saveAddress;
        this.msg = msg;
    }

    public String getLgTime() {
        return LgTime;
    }

    public void setLgTime(String lgTime) {
        LgTime = lgTime;
    }

    /**
     * 默认图片保存在resources/BigImg路径下
     * 默认图片格式是png
     */
    public ImgFormat() {
        fileName = defaultName;
        imageFormat = defaultImageFormat;
    }

//    添加自定义路径，自定义图片格式
    public ImgFormat(String s, String format) {
        fileName = s;
        imageFormat = format;
    }

    public ImgFormat(String fileName, String defaultName, String imageFormat, String lgTime, String defaultImageFormat, String saveAddress, String msg) {
        this.fileName = fileName;
        this.defaultName = defaultName;
        this.imageFormat = imageFormat;
        LgTime = lgTime;
        this.defaultImageFormat = defaultImageFormat;
        this.saveAddress = saveAddress;
        this.msg = msg;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getDefaultName() {
        return defaultName;
    }

    public void setDefaultName(String defaultName) {
        this.defaultName = defaultName;
    }

    public String getImageFormat() {
        return imageFormat;
    }

    public void setImageFormat(String imageFormat) {
        this.imageFormat = imageFormat;
    }

    public String getDefaultImageFormat() {
        return defaultImageFormat;
    }

    public void setDefaultImageFormat(String defaultImageFormat) {
        this.defaultImageFormat = defaultImageFormat;
    }

    public String getSaveAddress() {
        return saveAddress;
    }

    public void setSaveAddress(String saveAddress) {
        this.saveAddress = saveAddress;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
