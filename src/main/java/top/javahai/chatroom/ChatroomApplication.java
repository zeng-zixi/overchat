package top.javahai.chatroom;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * @author ww
 */
@SpringBootApplication
@MapperScan("top.javahai.chatroom.dao")
public class ChatroomApplication {
  public static void main(String[] args) {
    SpringApplicationBuilder builder = new SpringApplicationBuilder(ChatroomApplication.class);
    builder.headless(false);
    ConfigurableApplicationContext context = builder.run(args);
  }
}
