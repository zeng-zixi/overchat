package top.javahai.chatroom.service;

import top.javahai.chatroom.entity.ImgFormat;

import java.util.List;

public interface ScreenShotService {

//    对屏幕进行拍照
    List<Object> snapShot(ImgFormat imgFormat);
}
